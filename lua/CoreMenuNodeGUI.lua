core:module("CoreMenuNodeGui")
core:import("CoreUnit")
function NodeGui:_setup_item_rows(node)
	local items = node:items()
	local i = 0

	for _, item in pairs(items) do
		if item:visible() then
			item:parameters().gui_node = self
			local item_name = item:parameters().name
			local item_text = "menu item missing 'text_id'"

			if item:parameters().no_text then
				item_text = nil
			end

			local help_text = nil
			local params = item:parameters()

			if params.text_id then
				if self.localize_strings and params.localize ~= false and params.localize ~= "false" then
					item_text = managers.localization:text(params.text_id)
				else
					item_text = params.text_id
				end
			end

			if params.help_id then
				if self.localize_strings and params.localize_help ~= false and params.localize_help ~= "false" then
					help_text = managers.localization:text(params.help_id)
				else
					help_text = params.help_id
				end
			end

			local row_item = {}

			table.insert(self.row_items, row_item)

			row_item.item = item
			row_item.node = node
			row_item.node_gui = self
			row_item.type = item._type
			row_item.name = item_name
			row_item.position = {
				x = 0,
				y = self.font_size * i + self.spacing * (i - 1)
			}
			row_item.color = params.color or self.row_item_color
			row_item.row_item_color = params.row_item_color
			row_item.hightlight_color = params.hightlight_color
			row_item.disabled_color = params.disabled_color or self.row_item_disabled_text_color
			row_item.marker_color = params.marker_color or self.marker_color
			row_item.marker_disabled_color = params.marker_disabled_color or self.marker_disabled_color
			row_item.font = params.font or self.font
			row_item.font_size = params.font_size or self.font_size
			row_item.text = item_text
			row_item.help_text = help_text
			row_item.align = params.align or self.align or "left"
			row_item.halign = params.halign or self.halign or "left"
			row_item.vertical = params.vertical or self.vertical or "center"
			row_item.to_upper = params.to_upper == nil and self.to_upper or params.to_upper or false
			row_item.color_ranges = params.color_ranges or self.color_ranges or nil

			self:_create_menu_item(row_item)
			self:reload_item(item)

			i = i + 1
		end
	end

	self:_setup_size()
	self:scroll_setup()
    self:_flash_background_setup()
	self:_set_item_positions()

	self._highlighted_item = nil
end
function NodeGui:_reposition_items(highlighted_row_item)
	self.spacing = 0
	self.height_padding = 0
	local safe_rect = managers.viewport:get_safe_rect_pixels()
	local dy = 0

	if highlighted_row_item then
		if highlighted_row_item.item:parameters().back or highlighted_row_item.item:parameters().pd2_corner then
			return
		end

		local prev_item, first_item = nil
		local top_dividers_padding = 0
		local bottom_dividers_padding = 0
		local num_dividers_top = 0

		for i = 1, #self.row_items do
			first_item = self.row_items[i]

			if first_item.type ~= "divider" and not first_item.item:parameters().back and not first_item.item:parameters().pd2_corner then
				break
			elseif first_item.type == "divider" then
				top_dividers_padding = top_dividers_padding + (first_item.item:get_h(first_item, self) or first_item.gui_panel:h())
				num_dividers_top = num_dividers_top + 1
			end
		end

		local first = first_item.gui_panel == highlighted_row_item.gui_panel
		local last_item = nil
		local num_dividers_bottom = 0

		for i = #self.row_items, 1, -1 do
			last_item = self.row_items[i]

			if last_item.type ~= "divider" and not last_item.item:parameters().back and not last_item.item:parameters().pd2_corner then
				break
			elseif last_item.type == "divider" then
				bottom_dividers_padding = bottom_dividers_padding + (last_item.item:get_h(last_item, self) or last_item.gui_panel:h())
				num_dividers_bottom = num_dividers_bottom + 1
			end
		end

		local last = last_item.gui_panel == highlighted_row_item.gui_panel
		local prev_item, next_item = nil

		for i, row_item in ipairs(self.row_items) do
			if row_item.gui_panel == highlighted_row_item.gui_panel then
				if not first then
					for index = i - 1, 1, -1 do
						row_item = self.row_items[index]

						if row_item.type ~= "divider" and not row_item.item:parameters().back and not row_item.item:parameters().pd2_corner then
							prev_item = row_item

							break
						end
					end
				end

				if not last then
					for index = i + 1, #self.row_items do
						row_item = self.row_items[index]

						if row_item.type ~= "divider" and not row_item.item:parameters().back and not row_item.item:parameters().pd2_corner then
							next_item = row_item

							break
						end
					end
				end

				break
			end
		end

		local panel_height = self._main_panel:h()
		local panel_top = self._main_panel:world_y()
		local panel_bottom = panel_top + panel_height
		local highlighted_height = highlighted_row_item.item:get_h(highlighted_row_item, self) or highlighted_row_item.gui_panel:h()
		local highlighted_top = highlighted_row_item.gui_panel:world_y()
		local highlighted_bottom = highlighted_top + highlighted_height
		local offset_prev = (first and top_dividers_padding or 0) + self.height_padding

		if prev_item then
			local prev_top = prev_item.gui_panel:world_y()
			offset_prev = offset_prev + math.abs(prev_top - highlighted_top)
		end

		local offset_next = (last and bottom_dividers_padding or 0) + self.height_padding

		if next_item then
			local next_height = next_item.item:get_h(next_item, self) or next_item.gui_panel:h()
			local next_top = next_item.gui_panel:world_y()
			offset_next = offset_next + math.abs(next_top + next_height - highlighted_bottom)
		end

		if panel_top > highlighted_top - offset_prev then
			dy = -(highlighted_top - panel_top - offset_prev)
		elseif panel_bottom < highlighted_bottom + offset_next then
			dy = -(highlighted_bottom - panel_bottom + offset_next)
		end

		local old_dy = self._scroll_data.dy_left
		local is_same_dir = math.abs(old_dy) > 0 and (math.sign(dy) == math.sign(old_dy) or dy == 0)

		if is_same_dir then
			local within_view = math.within(highlighted_top, panel_top, panel_bottom)

			if within_view then
				dy = math.max(math.abs(old_dy), math.abs(dy)) * math.sign(old_dy)
			end
		end
	end

	self:scroll_start(dy)
end