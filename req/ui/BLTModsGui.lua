---@class BLTModsGui
---@field new fun(self, parent_panel):BLTModsGui
BLTModsGui = BLTModsGui or blt_class(BLTGUIBase)
BLTModsGui.last_y_position = 0
BLTModsGui.show_libraries = false
BLTModsGui.show_mod_icons = true
BLTModsGui.save_data_loaded = false

local padding = BLTUIData.padding.general

local function make_fine_text(text)
	local x, y, w, h = text:text_rect()
	text:set_world_shape(math.round(text:world_x()), math.round(text:world_y()), w, h)
end

local function make_fine_text_aligning(text)
	-- Make fine text, but use the text rect X and Y in set_position
	local x, y, w, h = text:text_rect()
	text:set_world_shape(math.round(x), math.round(y), w, h)
end

function BLTModsGui:close()
	BLTModsGui.last_y_position = self._scroll:canvas():y() * -1

	BLTModsGui.super.close(self)
end

function BLTModsGui:panel()
	return self._mods_panel
end

function BLTModsGui:_setup()
	self._mods_panel = self._parent_panel:panel({
		layer = self._layer
	})

	local background = self._mods_panel:rect({
		color = BLTUIData.colors.background,
		alpha = 0.4,
	})

	BLTBoxGuiObject:new(self._mods_panel, {
		sides = {1, 1, 1, 1},
		layer = 2
	})

	BLTBoxGuiObject:new(self._mods_panel, {
		sides = {1, 1, 2, 2},
		layer = 2
	})

	local show_text = managers.localization:exists("menu_button_show") and managers.localization:text("menu_button_show") or "SHOW"
	local hide_text = managers.localization:exists("menu_button_hide") and managers.localization:text("menu_button_hide") or "HIDE"

	local params = {
		x = 0,
		y = padding,
		width = self._mods_panel:w() - padding,
		height = BLTUIData.font_sizes.small,
		color = BLTUIData.colors.button,
		font = BLTUIData.fonts.small,
		font_size = BLTUIData.font_sizes.small,
		vertical = "bottom",
		align = "right",
		layer = 1
	}

	local function customize(changes)
		local t = table.map_copy(params)
		for key, value in pairs(changes) do
			t[key] = value
		end

		return t
	end

	-- Set up the toggle icons button
	local icons_text = self._mods_panel:text(customize({
		text = managers.localization:text("blt_mod_icons"),
		color = BLTUIData.colors.text
	}))

	-- Shift the show and hide buttons to the left of the label
	make_fine_text_aligning(icons_text)
	params.width = icons_text:x() - params.x - 4 -- 4px padding

	self._mod_icons_show_button = self._mods_panel:text(customize({
		text = show_text
	}))

	self._mod_icons_hide_button = self._mods_panel:text(customize({
		text = hide_text
	}))

	make_fine_text_aligning(self._mod_icons_show_button)
	make_fine_text_aligning(self._mod_icons_hide_button)

	params.width = self._mods_panel:w() - padding
	params.y = params.y + params.height

	-- Toggle libraries visible button

	-- Count the number of libraries installed
	local libs_count = 0
	for i, mod in ipairs(BLT.Mods:Mods()) do
		if mod:IsLibrary() then
			libs_count = libs_count + 1
		end
	end

	-- Add the libraries label
	local libraries_text = self._mods_panel:text(customize({
		text = managers.localization:text("blt_libraries", {count = libs_count}),
		color = BLTUIData.colors.text
	}))

	-- Shift the show and hide buttons to the left of the libraries label
	make_fine_text_aligning(libraries_text)
	params.width = libraries_text:x() - params.x - 4 -- 4px padding

	self._libraries_show_button = self._mods_panel:text(customize({
		text = show_text
	}))

	self._libraries_hide_button = self._mods_panel:text(customize({
		text = hide_text
	}))

	make_fine_text_aligning(self._libraries_show_button)
	make_fine_text_aligning(self._libraries_hide_button)

	self._custom_buttons[self._libraries_show_button] = {
		clbk = function()
			BLTModsGui.show_libraries = true
			self:update_visible_mods()
			return true
		end
	}
	self._custom_buttons[self._libraries_hide_button] = {
		clbk = function()
			BLTModsGui.show_libraries = false
			self:update_visible_mods()
			return true
		end
	}

	self._custom_buttons[self._mod_icons_show_button] = {
		clbk = function()
			BLTModsGui.show_mod_icons = true
			self:update_visible_mods()
			return true
		end
	}
	self._custom_buttons[self._mod_icons_hide_button] = {
		clbk = function()
			BLTModsGui.show_mod_icons = false
			self:update_visible_mods()
			return true
		end
	}

	-- Mods scroller
	local scroll_panel = self._mods_panel:panel({
		h = self._mods_panel:h() - (BLTUIData.font_sizes.small * 2 + padding),
		y = BLTUIData.font_sizes.small * 2 + padding
	})
	self._scroll = ScrollablePanel:new(scroll_panel, "mods_scroll")

	table.insert(self._scrolls, self._scroll)

	self:update_visible_mods(BLTModsGui.last_y_position)

	local mods_gui = BLT.save_data.mods_gui
	if mods_gui then
		BLTModsGui.show_libraries = mods_gui.show_libraries
		BLTModsGui.show_mod_icons = mods_gui.show_mod_icons
	end
	BLTModsGui.save_data_loaded = true
end

function BLTModsGui:visibility_refresh()
	self:update_visible_mods(BLTModsGui.last_y_position)
end

function BLTModsGui:update_visible_mods(scroll_position)
	-- Update the show libraries and mod icons button
	self._libraries_show_button:set_visible(not BLTModsGui.show_libraries)
	self._libraries_hide_button:set_visible(BLTModsGui.show_libraries)

	self._mod_icons_show_button:set_visible(not BLTModsGui.show_mod_icons)
	self._mod_icons_hide_button:set_visible(BLTModsGui.show_mod_icons)

	-- Save the position of the scroll panel
	BLTModsGui.last_y_position = scroll_position or self._scroll:canvas():y() * -1

	-- Clear the scroll panel
	self._scroll:canvas():clear()
	self._scroll:update_canvas_size() -- Ensure the canvas always starts at it's maximum size
	self._buttons = {}

	-- Create download manager button
	local title_text = managers.localization:text("blt_download_manager")
	local downloads_count = table.size(BLT.Downloads:pending_downloads())
	if downloads_count > 0 then
		title_text = title_text .. " (" .. BLTUIHelpers.nice_number_string(downloads_count) .. ")"
	end

	local icon = BLTUIData.icons.mod_auto_updates.texture
	local rect = BLTUIData.icons.mod_auto_updates.texture_rect
	local button = BLTUIButton:new(self._scroll:canvas(), {
		x = 0,
		y = 0,
		w = (self._scroll:canvas():w() - (BLTModItem.layout.x + 1) * BLTUIData.padding.mods) / BLTModItem.layout.x,
		h = 256 + (BLTModsGui.show_mod_icons and 0 or BLTUIData.padding.mods),
		title = title_text,
		text = managers.localization:text("blt_download_manager_help"),
		image = icon,
		image_size = 108,
		texture_rect = rect,
		callback = callback(self, self, "clbk_open_download_manager")
	}, self._scroll:panel())
	table.insert(self._buttons, button)

	-- Sort mods by library and name
	local mods = table.sorted_copy(BLT.Mods:Mods(), function (mod1, mod2)
		if mod1:GetId() == "base" then
			return true
		elseif mod2:GetId() == "base" then
			return false
		elseif mod1:IsLibrary() ~= mod2:IsLibrary() then
			return mod1:IsLibrary() and true or false
		elseif mod1:GetName():lower() < mod2:GetName():lower() then
			return true
		elseif mod1:GetName():lower() > mod2:GetName():lower() then
			return false
		end
		return mod1:GetId():lower() < mod2:GetId():lower()
	end)

	-- Create mod boxes
	for _, mod in ipairs(mods) do
		if BLTModsGui.show_libraries or not mod:IsLibrary() then
			local i = #self._buttons + 1

			-- Wrap mods around the download button, if mod icons are disabled
			if i >= 5 and not BLTModsGui.show_mod_icons then
				i = i + 1
			end

			local item = BLTModItem:new(self._scroll:canvas(), i, mod, BLTModsGui.show_mod_icons, self._scroll:panel())
			table.insert(self._buttons, item)
		end
	end
	
	-- Update scroll size
	self._scroll:update_canvas_size()

	self._scroll:scroll_to(BLTModsGui.last_y_position)
end

function BLTModsGui:clbk_open_download_manager()
	managers.menu:open_node("blt_download_manager")
	managers.menu:post_event("menu_enter")
end

Hooks:Add("BLTOnSaveData", "BLTOnSaveData.BLTModsGui", function(save_data)
	-- Special case - if the user never entered the BLT mod manager but changed BLT settings via mod options menu
	-- the data for BLTModsGui is not set from the save data, so only save mods gui data when it has been opened before
	if BLTModsGui.save_data_loaded then
		save_data.mods_gui = {
			show_libraries = BLTModsGui.show_libraries,
			show_mod_icons = BLTModsGui.show_mod_icons
		}
	end
end)
