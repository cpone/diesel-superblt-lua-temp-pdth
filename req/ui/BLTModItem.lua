BLT:Require("req/ui/BLTBoxGuiObject")

---@class BLTModItem
---@field new fun(self, panel, index, mod, show_icon):BLTModItem
BLTModItem = BLTModItem or blt_class()
BLTModItem.last_opened_mod = nil

local padding = BLTUIData.padding.mods

local function make_fine_text(text)
	local x, y, w, h = text:text_rect()
	text:set_world_shape(math.round(text:world_x()), math.round(text:world_y()), w, h)
end

BLTModItem.layout = {
	x = 4,
	y = 4
}
BLTModItem.image_size = 108

function BLTModItem:init(panel, index, mod, show_icon, container_panel)
	self._container_panel = container_panel

	local w = (panel:w() - (self.layout.x + 1) * padding) / self.layout.x
	local h = show_icon and 256 or 128
	local column, row = self:_get_col_row(index)
	local icon_size = 32

	self._mod = mod

	local bg_color = mod:GetColor()
	local text_color = BLTUIData.colors.title
	if mod:LastError() then
		bg_color = BLTUIData.colors.important
		text_color = BLTUIData.colors.important
	end

	-- Main panel
	self._panel = panel:panel({
		x = (w + padding) * (column - 1),
		y = (h + padding) * (row - 1),
		w = w,
		h = h
	})

	-- Background
	self._background = self._panel:rect({
		color = bg_color,
		alpha = mod:IsEnabled() and 0.2 or 0.05,
		blend_mode = "add",
		layer = -1
	})
	BLTBoxGuiObject:new(self._panel, {sides = {1, 1, 1, 1}})

	self._panel:bitmap({
		texture = "guis/textures/test_blur_df",
		w = self._panel:w(),
		h = self._panel:h(),
		render_template = "VertexColorTexturedBlur3D",
		layer = -1,
		halign = "scale",
		valign = "scale"
	})

	local alpha = mod:IsEnabled() and 1 or 0.5

	-- Mod name
	local mod_name = self._panel:text({
		name = "mod_name",
		font_size = BLTUIData.font_sizes.medium,
		font = BLTUIData.fonts.medium,
		layer = 10,
		blend_mode = "add",
		color = text_color,
		alpha = alpha,
		text = mod:GetName(),
		align = "center",
		vertical = "top",
		wrap = true,
		word_wrap = true
	})
	make_fine_text(mod_name)
	local name_padding = show_icon and padding or (icon_size + 4 + padding)
	mod_name:set_x(name_padding)
	mod_name:set_width(self._panel:w() - mod_name:x() - name_padding)
	mod_name:set_top(self._panel:h() * (show_icon and 0.5 or 0.1))

	-- Mod description
	local mod_desc = self._panel:text({
		name = "mod_desc",
		font_size = BLTUIData.font_sizes.small,
		font = BLTUIData.fonts.small,
		layer = 10,
		blend_mode = "add",
		color = text_color,
		alpha = alpha,
		text = string.sub(mod:GetDescription(), 1, 120),
		align = "left",
		vertical = "top",
		wrap = true,
		word_wrap = true,
		w = self._panel:w() - padding * 2
	})
	make_fine_text(mod_desc)
	mod_desc:set_center_x(self._panel:w() * 0.5)
	mod_desc:set_top(mod_name:bottom() + 5)

	-- Mod image
	local image_path
	if show_icon and mod:HasModImage() then
		image_path = mod:GetModImage()
	end

	if image_path then
		local image = self._panel:bitmap({
			name = "image",
			texture = image_path,
			color = Color.white,
			alpha = alpha,
			layer = 10,
			w = BLTModItem.image_size,
			h = BLTModItem.image_size
		})
		image:set_center_x(self._panel:w() * 0.5)
		image:set_top(padding)
	elseif show_icon then
		local no_image_panel = self._panel:panel({
			w = BLTModItem.image_size,
			h = BLTModItem.image_size,
			alpha = alpha,
			layer = 10
		})
		no_image_panel:set_center_x(self._panel:w() * 0.5)
		no_image_panel:set_top(padding)

		BLTBoxGuiObject:new(no_image_panel, {sides = {1, 1, 1, 1}})

		local no_image_text = no_image_panel:text({
			name = "no_image_text",
			font_size = BLTUIData.font_sizes.small,
			font = BLTUIData.fonts.small,
			layer = 10,
			blend_mode = "add",
			color = BLTUIData.colors.title,
			text = managers.localization:text("blt_no_image"),
			align = "center",
			vertical = "center",
			w = no_image_panel:w(),
			h = no_image_panel:h()
		})
	end

	-- Mod settings
	local icon_y = padding

	if not mod:IsUndisablable() then
		local icon = BLTUIData.icons.mod_enabled.texture
		local rect = BLTUIData.icons.mod_enabled.texture_rect
		local icon_enabled = self._panel:bitmap({
			name = "",
			texture = icon,
			texture_rect = rect,
			color = Color.white,
			alpha = 1,
			layer = 10,
			w = icon_size,
			h = icon_size
		})
		icon_enabled:set_left(padding)
		icon_enabled:set_top(icon_y)
		icon_y = icon_y + icon_size + 4

		if mod:WasEnabledAtStart() then
			icon_enabled:set_alpha(mod:IsEnabled() and 1 or 0.2)
		else
			icon_enabled:set_alpha(mod:IsEnabled() and 1 or 0.2)
			icon_enabled:set_color(mod:IsEnabled() and Color.yellow or Color.white)
		end
	end

	if mod:HasUpdates() then
		local icon = BLTUIData.icons.mod_auto_updates.texture
		local rect = BLTUIData.icons.mod_auto_updates.texture_rect
		local icon_updates = self._panel:bitmap({
			texture = icon,
			texture_rect = rect,
			color = Color.white,
			alpha = mod:AreUpdatesEnabled() and 1 or 0.2,
			layer = 10,
			w = icon_size,
			h = icon_size
		})
		if show_icon then
			icon_updates:set_left(padding)
			icon_updates:set_top(icon_y)
		else
			icon_updates:set_right(self._panel:w() - padding)
			icon_updates:set_top(padding)
		end

		-- Animate the icon. When the update is done, the animation ends and
		-- sets the icon to the appropriate colour
		icon_updates:animate(callback(self, self, "_clbk_animate_update_icon"))
	end
end

function BLTModItem:_clbk_animate_update_icon(icon)
	-- While the update is still in progress, fade the icon
	local time = 0
	while self._mod:IsCheckingForUpdates() do
		local dt = coroutine.yield()
		time = time + dt

		-- Fade colour from 0 to 1 to 0 over the course of two seconds
		local colour = time % 2 -- From 0-2

		if colour > 1 then
			-- If the colour has gone past half way, subtract it from two. This
			-- causes it to decrease starting from 1 (as 2-1=1) to 0 (as 2-2=0).
			colour = 2 - colour
		end

		-- Lerb between white and blue to make it fade in and out
		icon:set_color(math.lerp(Color.white, Color.blue, colour))
	end

	-- Check for corrupted downloads, and set the colour accordingly
	if self._mod:GetUpdateError() then
		icon:set_color(Color.red)
		return
	end

	-- Check if the update is resolved
	if BLT.Downloads:get_pending_downloads_for(self._mod) then
		icon:set_color(Color.yellow)
		return
	end

	-- Update check finished and no updates are due, colour it white
	icon:set_color(Color.white)
end

function BLTModItem:_get_col_row(index)
	local column = 1
	local row = 1
	for i = 1, index - 1 do
		column = column + 1
		if column > self.layout.x then
			row = row + 1
			column = 1
		end
	end
	return column, row
end

function BLTModItem:inside(x, y)
	return (not self._container_panel or self._container_panel:inside(x, y)) and self._panel:inside(x, y)
end

function BLTModItem:mod()
	return self._mod
end

function BLTModItem:mouse_moved(x, y)
	local used = false
	local pointer = "arrow"

	local inside = self:inside(x, y)
	if self._highlight ~= inside then
		self._background:set_alpha(inside and 0.4 or self._mod:IsEnabled() and 0.2 or 0.05)
		if inside then
			managers.menu:post_event("highlight")
			used, pointer = true, "link"
		end
		self._highlight = inside
	end

	return used, pointer
end

function BLTModItem:mouse_pressed(button, x, y)
	if button == Idstring("0") then -- left click
		if self:inside(x, y) then
			BLTModItem.last_opened_mod = self:mod()

			managers.menu:open_node("view_blt_mod")
			managers.menu:post_event("menu_enter")
		end
	end
end