---@class BLTModsComponent
---@field new fun(self, is_start_page, component_data):BLTModsComponent
BLTViewModComponent = BLTViewModComponent or blt_class(BLTGuiComponent)

function BLTViewModComponent:_setup(is_start_page, component_data)
	BLTViewModComponent.super._setup(self, is_start_page, component_data)

	self._child_gui = BLTViewModGui:new(self._panel, self._init_layer)
end

local component_name = "view_blt_mod"
MenuHelper:AddComponent(component_name, BLTViewModComponent)

if BLT:GetGame() == "raid" then
	Hooks:Add("MenuComponentManagerInitialize", component_name .. ".RaidUpdate.MenuComponentManagerInitialize", function(component)
		MenuComponentManager["create_" .. component_name .. "_gui_pre_update"] = MenuComponentManager["create_" .. component_name .. "_gui"]
		MenuComponentManager["create_" .. component_name .. "_gui"] = function(self, ...)
			local ret = MenuComponentManager["create_" .. component_name .. "_gui_pre_update"](self, ...)
			table.insert(self._update_components, ret)
			return ret
		end
	end)
end