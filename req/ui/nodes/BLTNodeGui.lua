---@class BLTNodeGui
---@field new fun(self, node, layer, parameters):BLTNodeGui
BLTNodeGui = BLTNodeGui or blt_class(MenuNodeGui)

function BLTNodeGui:init(node, layer, parameters)
	BLTNodeGui.super.init(self, node, layer, parameters)

	self._blt_panel = self._main_panel or self._item_panel_parent
	self._node = node

	self:_setup()

	self:_activate_mouse()
end

function BLTNodeGui:_activate_mouse()
	if not self._mouse_active then
		local data = {}
		data.mouse_move = callback(self, self, "mouse_moved")
		data.mouse_press = callback(self, self, "mouse_pressed")
		data.mouse_release = callback(self, self, "mouse_released")
		data.mouse_click = callback(self, self, "mouse_clicked")
		data.id = "blt_node_gui_" .. tostring(self.layers.first)

		self._mouse_active = true

		managers.mouse_pointer:use_mouse(data)
	end
end

function BLTNodeGui:update(t, dt)
	BLTNodeGui.super.update(self, t, dt)

	local x, y = managers.mouse_pointer:world_position()
	if self._mouse_active then
		self:mouse_moved(managers.mouse_pointer:mouse(), x, y)
	end

	if self._child_gui then
		if self.ws:visible() then
			local inside_child_gui = self._child_gui.panel and alive(self._child_gui:panel()) and self._child_gui:panel():inside(x, y)
			if inside_child_gui then
				self:_activate_mouse()
			else
				self:_deactivate_mouse()
			end
		end

		if self._child_gui.update then
			self._child_gui:update(t, dt)
		end
	end
end

function BLTNodeGui:set_visible(visible)
	BLTNodeGui.super.set_visible(self, visible)
	self:visibility_refresh()

	if visible then
		self:_activate_mouse()
	else
		self:_deactivate_mouse()
	end
end

function BLTNodeGui:visibility_refresh()
	if self._child_gui and self._child_gui.visibility_refresh then
		self._child_gui:visibility_refresh()
	end
end

function BLTNodeGui:_deactivate_mouse()
	if self._mouse_active then
		self._mouse_active = false

		managers.mouse_pointer:remove_mouse("blt_node_gui_" .. tostring(self.layers.first))
	end
end

function BLTNodeGui:close()
	self:_deactivate_mouse()

	if self._child_gui and self._child_gui.close then
		self._child_gui:close()
	end

	BLTNodeGui.super.close(self)
end

function BLTNodeGui:_setup()
end

function BLTNodeGui:mouse_moved(o, x, y)
	if self._child_gui and self._child_gui.mouse_moved then
		self._child_gui:mouse_moved(x, y)
	end
end

function BLTNodeGui:mouse_clicked(o, button, x, y)
	if self._child_gui and self._child_gui.mouse_clicked then
		self._child_gui:mouse_clicked(button, x, y)
	end
end

function BLTNodeGui:mouse_pressed(o, button, x, y)
	if self._child_gui and self._child_gui.mouse_pressed then
		self._child_gui:mouse_pressed(button, x, y)
	end
end

function BLTNodeGui:mouse_released(o, button, x, y)
	if self._child_gui and self._child_gui.mouse_released then
		self._child_gui:mouse_released(button, x, y)
	end
end