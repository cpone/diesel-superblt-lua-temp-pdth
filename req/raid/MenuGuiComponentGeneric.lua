MenuGuiComponentGeneric = MenuGuiComponentGeneric or class()
function MenuGuiComponentGeneric:init(ws, fullscreen_ws, node, name)
	self._ws = ws
	self._fullscreen_ws = fullscreen_ws
	self._init_layer = self._ws:panel():layer()
	self._fullscreen_panel = self._fullscreen_ws:panel():panel({})

	self._node = node

	local component_data = self._node:parameters().menu_component_data
	local is_start_page = not component_data and true or false

	self:_setup(is_start_page, component_data)
	self:set_layer(10)


	if self._node.components.raid_menu_header then
		self._node.components.raid_menu_header:set_screen_name(node:parameters().topic_id)
	end

	if self._node.components.raid_menu_footer then
		self._node.components.raid_menu_footer:hide_name_and_gold_panel()
	end

	managers.raid_menu:set_legend_labels({
		controller = {
			"menu_legend_back",
			"menu_legend_default_options"
		},
		keyboard = {
			{
				key = "footer_back",
				callback = function()
					managers.menu:active_menu().logic:navigate_back(true)
				end
			}
		}
	})
end

function MenuGuiComponentGeneric:set_layer(layer)
	if alive(self._panel) then
		self._panel:set_layer(self._init_layer + layer)
	end
end

function MenuGuiComponentGeneric:_start_page_data()
	return {}
end

function MenuGuiComponentGeneric:_setup(is_start_page, component_data)
	self._data = component_data or self:_start_page_data()

	if alive(self._panel) then
		self._ws:panel():remove(self._panel)
	end

	self._panel = self._ws:panel():panel({
		layer = self._init_layer
	})
end


function MenuGuiComponentGeneric:close()
	self._ws:panel():remove(self._panel)
	self._fullscreen_ws:panel():remove(self._fullscreen_panel)
end

function MenuGuiComponentGeneric:mouse_pressed(o, button, x, y)
end

function MenuGuiComponentGeneric:back_pressed()
end

function MenuGuiComponentGeneric:update(t, dt)
end

function MenuGuiComponentGeneric:mouse_moved(o, x, y)
end

function MenuGuiComponentGeneric:mouse_clicked(o, button, x, y)
end

function MenuGuiComponentGeneric:mouse_released(o, button, x, y)
end

function MenuGuiComponentGeneric:mouse_wheel_up(x, y)
end

function MenuGuiComponentGeneric:mouse_wheel_down(x, y)
end

function MenuGuiComponentGeneric:mouse_double_click(o, button, x, y)
end

function MenuGuiComponentGeneric:make_fine_text(text)
	if not alive(text) then
		return
	end

	local x,y,w,h = text:text_rect()
	text:set_size(w, h)
	text:set_position(math.round(text:x()), math.round(text:y()))
end

function MenuGuiComponentGeneric:move_up()
end

function MenuGuiComponentGeneric:move_down()
end

function MenuGuiComponentGeneric:move_left()
end

function MenuGuiComponentGeneric:move_right()
end