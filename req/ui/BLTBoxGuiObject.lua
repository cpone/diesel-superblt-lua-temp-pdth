BLTBoxGuiObject = BLTBoxGuiObject or blt_class()
local mvector_tl = Vector3()
local mvector_tr = Vector3()
local mvector_bl = Vector3()
local mvector_br = Vector3()

function BLTBoxGuiObject:init(panel, config)
	self:create_sides(panel, config)
end

function BLTBoxGuiObject:refresh()
	self:clear_sides()
	self:create_sides()
end

function BLTBoxGuiObject:create_sides(panel, config)
	self._config = config or self._config or {}
	self._parent_panel = panel or self._parent_panel

	if not alive(self._parent_panel) then
		Application:error("[BLTBoxGuiObject:create_sides] Failed creating BoxGui. Parent panel not alive!")
		Application:stack_dump()

		return
	end

	if alive(self._panel) then
		self._panel:parent():remove(self._panel)

		self._panel = nil
	end

	self._panel = self._parent_panel:panel({
		halign = "scale",
		valign = "scale",
		name   = self._config.name or "BoxGuiObject" .. tostring(self._parent_panel:num_children()),
		w      = self._config.w,
		h      = self._config.h,
		layer  = self._config.layer or 1
	})

	self._color = self._config.color or self._color or Color.white

	local left_side   = self._config.sides and self._config.sides[1] or self._config.left or 0
	local right_side  = self._config.sides and self._config.sides[2] or self._config.right or 0
	local top_side    = self._config.sides and self._config.sides[3] or self._config.top or 0
	local bottom_side = self._config.sides and self._config.sides[4] or self._config.bottom or 0

	self._side_1 = self:_create_side(self._panel, "left",   left_side,   self._config.texture)
	self._side_2 = self:_create_side(self._panel, "right",  right_side,  self._config.texture)
	self._side_3 = self:_create_side(self._panel, "top",    top_side,    self._config.texture)
	self._side_4 = self:_create_side(self._panel, "bottom", bottom_side, self._config.texture)
end

function BLTBoxGuiObject:clear_sides()
	if self._panel and alive(self._panel) then
		if self._side_1 and alive(self._side_1) then
			self._panel:remove(self._side_1)
		end

		if self._side_2 and alive(self._side_2) then
			self._panel:remove(self._side_2)
		end

		if self._side_3 and alive(self._side_3) then
			self._panel:remove(self._side_3)
		end

		if self._side_4 and alive(self._side_4) then
			self._panel:remove(self._side_4)
		end
	end
end

local no_default_texture = BLT:GetGame() == "pdth" or BLT:GetGame() == "raid"

function BLTBoxGuiObject:_create_side(panel, side, type, texture)
	local ids_side = Idstring(side)
	local ids_left = Idstring("left")
	local ids_right = Idstring("right")
	local ids_top = Idstring("top")
	local ids_bottom = Idstring("bottom")
	local left_or_right = false
	local w, h = nil

	if ids_side == ids_left or ids_side == ids_right then
		left_or_right = true
		w = 2
		h = panel:h()
	else
		w = panel:w()
		h = 2
	end

	local side_panel = panel:panel({
		name = side,
		w = w,
		h = h,
		halign = left_or_right and side or "scale",
		valign = left_or_right and "scale" or side
	})

	if not texture and not no_default_texture then
		texture = "guis/textures/pd2/shared_lines"
	end

	if type == 0 then
		return
	elseif type == 1 or type == 3 or type == 4 then
		if texture then
			local one = side_panel:bitmap({
				wrap_mode = "wrap",
				texture = texture
			})
			local two = side_panel:bitmap({
				wrap_mode = "wrap",
				texture = texture
			})
			local x = math.random(1, 255)
			local y = math.random(0, one:texture_height() / 2 - 1) * 2
			local tw = math.min(10, w)
			local th = math.min(10, h)

			if left_or_right then
				one:set_halign(side)
				two:set_halign(side)
				one:set_valign("scale")
				two:set_valign("scale")
				mvector3.set_static(mvector_tl, x, y + tw, 0)
				mvector3.set_static(mvector_tr, x, y, 0)
				mvector3.set_static(mvector_bl, x + th, y + tw, 0)
				mvector3.set_static(mvector_br, x + th, y, 0)
				one:set_texture_coordinates(mvector_tl, mvector_tr, mvector_bl, mvector_br)

				x = math.random(1, 255)
				y = math.random(0, math.round(one:texture_height() / 2 - 1)) * 2

				mvector3.set_static(mvector_tl, x, y + tw, 0)
				mvector3.set_static(mvector_tr, x, y, 0)
				mvector3.set_static(mvector_bl, x + th, y + tw, 0)
				mvector3.set_static(mvector_br, x + th, y, 0)
				two:set_texture_coordinates(mvector_tl, mvector_tr, mvector_bl, mvector_br)
				one:set_size(2, th)
				two:set_size(2, th)
				two:set_bottom(h)
			else
				one:set_halign("scale")
				two:set_halign("scale")
				one:set_valign(side)
				two:set_valign(side)
				mvector3.set_static(mvector_tl, x, y, 0)
				mvector3.set_static(mvector_tr, x + tw, y, 0)
				mvector3.set_static(mvector_bl, x, y + th, 0)
				mvector3.set_static(mvector_br, x + tw, y + th, 0)
				one:set_texture_coordinates(mvector_tl, mvector_tr, mvector_bl, mvector_br)

				x = math.random(1, 255)
				y = math.random(0, math.round(one:texture_height() / 2 - 1)) * 2

				mvector3.set_static(mvector_tl, x, y, 0)
				mvector3.set_static(mvector_tr, x + tw, y, 0)
				mvector3.set_static(mvector_bl, x, y + th, 0)
				mvector3.set_static(mvector_br, x + tw, y + th, 0)
				two:set_texture_coordinates(mvector_tl, mvector_tr, mvector_bl, mvector_br)
				one:set_size(tw, 2)
				two:set_size(tw, 2)
				two:set_right(w)
			end

			one:set_visible(type == 1 or type == 3)
			two:set_visible(type == 1 or type == 4)
		else
			local one = side_panel:rect({
				color = Color.white
			})
			local two = side_panel:rect({
				color = Color.white
			})

			local tw = math.min(10, w)
			local th = math.min(10, h)

			if left_or_right then
				one:set_halign(side)
				two:set_halign(side)
				one:set_valign("scale")
				two:set_valign("scale")

				one:set_size(2, th)
				two:set_size(2, th)
				two:set_bottom(h)
			else
				one:set_size(tw, 2)
				two:set_size(tw, 2)
				two:set_right(w)
			end

			one:set_visible(type == 1 or type == 3)
			two:set_visible(type == 1 or type == 4)
		end
	elseif type == 2 then
		if texture then
			local full = side_panel:bitmap({
				wrap_mode = "wrap",
				texture = texture,
				w = side_panel:w(),
				h = side_panel:h()
			})
			local x = math.random(1, 255)
			local y = math.random(0, math.round(full:texture_height() / 2 - 1)) * 2

			if left_or_right then
				full:set_halign(side)
				full:set_valign("scale")
				mvector3.set_static(mvector_tl, x, y + w, 0)
				mvector3.set_static(mvector_tr, x, y, 0)
				mvector3.set_static(mvector_bl, x + h, y + w, 0)
				mvector3.set_static(mvector_br, x + h, y, 0)
				full:set_texture_coordinates(mvector_tl, mvector_tr, mvector_bl, mvector_br)
			else
				full:set_halign("scale")
				full:set_valign(side)
				mvector3.set_static(mvector_tl, x, y, 0)
				mvector3.set_static(mvector_tr, x + w, y, 0)
				mvector3.set_static(mvector_bl, x, y + h, 0)
				mvector3.set_static(mvector_br, x + w, y + h, 0)
				full:set_texture_coordinates(mvector_tl, mvector_tr, mvector_bl, mvector_br)
			end
		else
			local full = side_panel:rect({
				color = Color.white,
				w = side_panel:w(),
				h = side_panel:h()
			})
		end
	else
		Application:error("[BoxGuiObject] Type", type, "is not supported")
		Application:stack_dump()

		return
	end

	side_panel:set_position(0, 0)

	if ids_side == ids_right then
		side_panel:set_right(panel:w())
	elseif ids_side == ids_bottom then
		side_panel:set_bottom(panel:h())
	end

	return side_panel
end

function BLTBoxGuiObject:hide()
	self._panel:hide()
end

function BLTBoxGuiObject:show()
	self._panel:show()
end

function BLTBoxGuiObject:set_visible(visible)
	self._panel:set_visible(visible)
end

function BLTBoxGuiObject:visible()
	return self._panel:visible()
end

function BLTBoxGuiObject:set_layer(layer)
	self._panel:set_layer(layer)
end

function BLTBoxGuiObject:size()
	return self._panel:size()
end

function BLTBoxGuiObject:alive()
	return alive(self._panel)
end

function BLTBoxGuiObject:inside(x, y, side)
	if not self:alive() then
		return false
	end

	if side then
		return self._panel:child(side) and self._panel:child(side):inside(x, y)
	else
		return self._panel:inside(x, y)
	end
end

function BLTBoxGuiObject:set_aligns(halign, valign)
	for i, d in pairs(self._panel:children()) do
		d:set_valign(valign)
		d:set_halign(halign)
	end
end

function BLTBoxGuiObject:set_clipping(clip, rec_panel)
	for i, d in pairs(rec_panel and rec_panel:children() or self._panel:children()) do
		if d.set_rotation then
			d:set_rotation(clip and 0 or 360)
		else
			self:set_clipping(clip, d)
		end
	end
end

function BLTBoxGuiObject:color()
	return self._color
end

function BLTBoxGuiObject:set_color(color, rec_panel)
	self._color = color

	for i, d in pairs(rec_panel and rec_panel:children() or self._panel:children()) do
		if d.set_color then
			d:set_color(color)
		else
			self:set_color(color, d)
		end
	end
end

function BLTBoxGuiObject:blend_mode()
	return self._blend_mode
end

function BLTBoxGuiObject:set_blend_mode(blend_mode, rec_panel)
	self._blend_mode = blend_mode

	for i, d in pairs(rec_panel and rec_panel:children() or self._panel:children()) do
		if d.set_blend_mode then
			d:set_blend_mode(blend_mode)
		else
			self:set_blend_mode(blend_mode, d)
		end
	end
end

function BLTBoxGuiObject:close()
	if alive(self._panel) and alive(self._panel:parent()) then
		self._panel:parent():remove(self._panel)
	end
end