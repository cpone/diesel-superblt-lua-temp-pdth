core:module("CoreViewportManager")

function ViewportManager:resolution_changed()
    managers.gui_data:resolution_changed()
	if rawget(_G, "tweak_data").resolution_changed then
		rawget(_G, "tweak_data"):resolution_changed()
	end
	for i, svp in ipairs(self:viewports()) do
		svp:_resolution_changed(i)
	end
	self._resolution_changed_event_handler:dispatch()
end

function ViewportManager:get_safe_rect_pixels()
	local res = RenderSettings.resolution
	local safe_rect_scale = self:get_safe_rect()
	local safe_rect_pixels = {
		x = math.round(safe_rect_scale.x * res.x),
		y = math.round(safe_rect_scale.y * res.y),
		width = math.round(safe_rect_scale.width * res.x),
		height = math.round(safe_rect_scale.height * res.y)
	}

	return safe_rect_pixels
end