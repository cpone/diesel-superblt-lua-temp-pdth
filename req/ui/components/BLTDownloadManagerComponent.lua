---@class BLTDownloadManagerComponent
---@field new fun(self, is_start_page, component_data):BLTDownloadManagerComponent
BLTDownloadManagerComponent = BLTDownloadManagerComponent or blt_class(BLTGuiComponent)

function BLTDownloadManagerComponent:_setup(is_start_page, component_data)
	BLTDownloadManagerComponent.super._setup(self, is_start_page, component_data)

	self._child_gui = BLTDownloadManagerGui:new(self._panel, self._init_layer)
end

local component_name = "blt_download_manager"
MenuHelper:AddComponent(component_name, BLTDownloadManagerComponent)

if BLT:GetGame() == "raid" then
	Hooks:Add("MenuComponentManagerInitialize", component_name .. ".RaidUpdate.MenuComponentManagerInitialize", function(component)
		MenuComponentManager["create_" .. component_name .. "_gui_pre_update"] = MenuComponentManager["create_" .. component_name .. "_gui"]
		MenuComponentManager["create_" .. component_name .. "_gui"] = function(self, ...)
			local ret = MenuComponentManager["create_" .. component_name .. "_gui_pre_update"](self, ...)
			table.insert(self._update_components, ret)
			return ret
		end
	end)
end