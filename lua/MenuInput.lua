core:import("CoreMenuInput")

Hooks:PostHook(MenuInput, "init", "BLT.MenuInput.init", function(self, ...)
    self._item_input_action_map[MenuItemDivider.TYPE] = callback(self, self, "input_item")
end)

Hooks:PostHook(MenuInput, "mouse_pressed", "BLT.MenuInput.mouse_pressed", function(self, o, button, x, y, ...)
    if button == Idstring("0") then
		local node_gui = managers.menu:active_menu().renderer:active_node_gui()

		if not node_gui or node_gui._listening_to_input then
			return
		end

		if node_gui and not node_gui.CUSTOM_MOUSE_INPUT then
			for _, row_item in pairs(node_gui.row_items) do
                if row_item.type == "divider" then
                    -- nothing
                end
            end
        end
    end
end)