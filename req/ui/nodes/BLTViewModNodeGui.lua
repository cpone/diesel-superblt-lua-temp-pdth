---@class BLTViewModNodeGui
---@field new fun(self, node, layer, parameters):BLTViewModNodeGui
BLTViewModNodeGui = BLTViewModNodeGui or blt_class(BLTNodeGui)

function BLTViewModNodeGui:_setup()
	self._child_gui = BLTViewModGui:new(self._blt_panel, self.layers.items)
end