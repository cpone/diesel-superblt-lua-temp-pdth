---@class BLTNotificationsComponent
---@field new fun(self, is_start_page, component_data):BLTNotificationsComponent
BLTNotificationsComponent = BLTNotificationsComponent or blt_class(BLTGuiComponent)

function BLTNotificationsComponent:_setup(is_start_page, component_data)
	BLTNotificationsComponent.super._setup(self, is_start_page, component_data)

	self._child_gui = BLTNotificationsGui:new(self._panel, self._init_layer,
		0,
		self._panel:height() - 128,
		524,
		128
	)
end

local component_name = "blt_notifications"
MenuHelper:AddComponent(component_name, BLTNotificationsComponent)

if BLT:GetGame() == "raid" then
	Hooks:Add("MenuComponentManagerInitialize", component_name .. ".RaidUpdate.MenuComponentManagerInitialize", function(component)
		MenuComponentManager["create_" .. component_name .. "_gui_pre_update"] = MenuComponentManager["create_" .. component_name .. "_gui"]
		MenuComponentManager["create_" .. component_name .. "_gui"] = function(self, ...)
			local ret = MenuComponentManager["create_" .. component_name .. "_gui_pre_update"](self, ...)
			table.insert(self._update_components, ret)
			return ret
		end
	end)
end