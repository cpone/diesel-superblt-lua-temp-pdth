---@class BLTNodeGui
---@field new fun(self, parent_panel):BLTNodeGui
BLTGUIBase = BLTGUIBase or blt_class()

function BLTGUIBase:init(parent_panel, layer)
	self._parent_panel = parent_panel
	self._layer = layer

	self._buttons = {}
	self._custom_buttons = {}

	self._scrolls = {}

	self:_setup()

	if not managers.menu:is_pc_controller() then
		managers.menu:active_menu().input:activate_controller_mouse()
	end
end

function BLTGUIBase:close()
	if not managers.menu:is_pc_controller() then
		managers.menu:active_menu().input:activate_controller_mouse()
	end
end

function BLTGUIBase:panel()
	return self._main_panel
end

function BLTGUIBase:buttons()
	return self._buttons
end

function BLTGUIBase:custom_buttons()
	return self._custom_buttons
end

function BLTGUIBase:scrolls()
	return self._scrolls
end

function BLTGUIBase:visibility_refresh()
end

function BLTGUIBase:_setup()
end

function BLTGUIBase:mouse_moved(x, y)
	local used = false
	local pointer = "arrow"

	for _, scroll in pairs(self._scrolls) do
		if alive(scroll) and not used then
			used, pointer = scroll:mouse_moved(x, y)
		end
	end

	for button, data in pairs(self._custom_buttons) do
		if alive(button) and button:visible() then
			if button:inside(x, y) then
				local colour = data.selected_colour or BLTUIData.colors.button_highlight
				if button:color() ~= colour then
					button:set_color(colour)
					managers.menu:post_event("highlight")
				end
				used, pointer = true, "link"
			else
				button:set_color(data.deselected_colour or BLTUIData.colors.button)
			end
		end
	end

	for _, item in pairs(self._buttons) do
		if not used then
			if item.mouse_moved then
				used, pointer = item:mouse_moved(x, y)
			end
		end
	end

	return used, pointer
end

function BLTGUIBase:mouse_clicked(button, x, y)
	for _, scroll in pairs(self._scrolls) do
		if alive(scroll) then
			scroll:mouse_clicked(button, x, y)
		end
	end

	for _, item in pairs(self._buttons) do
		if item.mouse_clicked then
			item:mouse_clicked(button, x, y)
		end
	end
end

function BLTGUIBase:mouse_pressed(button, x, y)
	for _, scroll in pairs(self._scrolls) do
		if alive(scroll) then
			scroll:mouse_pressed(button, x, y)
		end
	end

	if button == Idstring("0") then
		for button, data in pairs(self._custom_buttons) do
			if alive(button) and button:visible() and button:inside(x, y) then
				return data.clbk()
			end
		end
	elseif button == Idstring("mouse wheel down") then
		self:mouse_wheel_down(x, y)
	elseif button == Idstring("mouse wheel up") then
		self:mouse_wheel_up(x, y)
	end

	for _, item in pairs(self._buttons) do
		if item.mouse_pressed then
			item:mouse_pressed(button, x, y)
		end
	end

	return
end

function BLTGUIBase:mouse_released(button, x, y)
	for _, scroll in pairs(self._scrolls) do
		if alive(scroll) then
			scroll:mouse_released(button, x, y)
		end
	end

	for _, item in pairs(self._buttons) do
		if item.mouse_released then
			item:mouse_released(button, x, y)
		end
	end
end

function BLTGUIBase:mouse_wheel_up(x, y)
	for _, scroll in pairs(self._scrolls) do
		if alive(scroll) then
			scroll:scroll(x, y, 1)
		end
	end
end

function BLTGUIBase:mouse_wheel_down(x, y)
	for _, scroll in pairs(self._scrolls) do
		if alive(scroll) then
			scroll:scroll(x, y, -1)
		end
	end
end