---@class BLTNotificationsGui
---@field new fun(self, ws, fullscreen_ws, node):BLTNotificationsGui
BLTNotificationsGui = BLTNotificationsGui or blt_class()

local padding = BLTUIData.padding.general

-- Copied from NewHeistsGui
local SPOT_W = 32
local SPOT_H = 8
local BAR_W = 32
local BAR_H = 6
local BAR_X = (SPOT_W - BAR_W) / 2
local BAR_Y = 0
local TIME_PER_PAGE = 6
local CHANGE_TIME = 0.5

function BLTNotificationsGui:init(parent_panel, layer, x, y, w, h)
	self._main_panel = parent_panel
	self._layer = layer

	self._buttons = {}
	self._next_time = Application:time() + TIME_PER_PAGE

	self._current = 0
	self._notifications = {}
	self._notifications_count = 0

	self:_setup(x, y, w, h)

	BLT.Notifications:set_current_notifications_gui(self)

	self._notifications_ready = true
end

function BLTNotificationsGui:close()
	self._notifications_ready = false

	BLT.Notifications:set_current_notifications_gui(nil)
end

function BLTNotificationsGui:panel()
	return self._panel
end

function BLTNotificationsGui:_setup(x, y, w, h)
	local font = BLTUIData.fonts.small
	local font_size = BLTUIData.font_sizes.small
	local max_left_len = 0
	local max_right_len = 0
	local extra_w = font_size * 4
	local icon_size = 16

	self._enabled = true

	-- Create panels
	self._panel = self._main_panel:panel({
		x = x or 0,
		y = y or 0,
		w = w or 524,
		h = h or 128,
		layer = self._layer
	})

	-- BLTBoxGuiObject:new(self._panel:panel({ layer = 100 }), { sides = { 1, 1, 1, 1 } })

	self._content_panel = self._panel:panel({
		h = self._panel:h() * 0.8
	})

	self._buttons_panel = self._panel:panel({
		h = self._panel:h() * 0.2
	})

	self._buttons_panel:set_top(self._content_panel:h())

	local bg_rect = self._content_panel:rect({
		name = "background",
		color = BLTUIData.colors.background,
		alpha = 0.4,
		layer = -1,
		halign = "scale",
		valign = "scale"
	})

	-- Outline
	BLTBoxGuiObject:new(self._content_panel, {sides = {1, 1, 1, 1}})
	self._content_outline = BLTBoxGuiObject:new(self._content_panel, {sides = {2, 2, 2, 2}})

	-- Downloads notification
	self._downloads_panel = self._panel:panel({
		name = "downloads",
		w = 48,
		h = 48,
		layer = 100
	})

	local texture = BLTUIData.icons.mod_downloads.texture
	local rect = BLTUIData.icons.mod_downloads.texture_rect
	self._downloads_panel:bitmap({
		texture = texture,
		texture_rect = rect,
		w = self._downloads_panel:w(),
		h = self._downloads_panel:h(),
		color = Color.red
	})

	self._downloads_panel:rect({
		x = 38 / 2.5 - 2,
		y = 28 / 2.5,
		w = 54 / 2.5,
		h = 72 / 2.5 - 2,
		color = Color.red
	})

	self._downloads_count = self._downloads_panel:text({
		font_size = BLTUIData.font_sizes.medium,
		font = BLTUIData.fonts.medium,
		layer = 10,
		blend_mode = "add",
		color = BLTUIData.colors.title,
		text = "2",
		align = "center",
		vertical = "center"
	})

	self._downloads_panel:set_visible(false)

	-- Move other panels to fit the downloads notification in nicely
	self._panel:set_w(self._panel:w() + 24)
	self._panel:set_h(self._panel:h() + 24)
	self._panel:set_top(self._panel:top() - 15)
	self._content_panel:set_top(self._content_panel:top() + 24)
	self._buttons_panel:set_top(self._buttons_panel:top() + 24)

	self._downloads_panel:set_right(self._panel:w())
	self._downloads_panel:set_top(0)

	-- Add notifications that have already been registered
	for _, notif in ipairs(BLT.Notifications:get_notifications()) do
		self:add_notification(notif)
	end

	-- Check for updates when creating the notification UI as we show the check here
	BLT.Mods:RunAutoCheckForUpdates()
end

function BLTNotificationsGui:_rec_round_object(object)
	local x, y, w, h = object:shape()
	object:set_shape(math.round(x), math.round(y), math.round(w), math.round(h))
	if object.children then
		for i, d in ipairs(object:children()) do
			self:_rec_round_object(d)
		end
	end
end

function BLTNotificationsGui:_make_fine_text(text)
	local x, y, w, h = text:text_rect()
	text:set_size(w, h)
	text:set_position(math.round(text:x()), math.round(text:y()))
end

--------------------------------------------------------------------------------

function BLTNotificationsGui:_get_notification(uid)
	local idx
	for i, data in ipairs(self._notifications) do
		if data.id == uid then
			idx = i
			break
		end
	end
	return self._notifications[idx], idx
end

function BLTNotificationsGui:add_notification(parameters)
	-- Create notification panel
	local new_notif = self._content_panel:panel({})

	local icon_size = new_notif:h() - padding * 2
	local icon
	if parameters.icon then
		icon = new_notif:bitmap({
			texture = parameters.icon,
			texture_rect = parameters.icon_texture_rect,
			color = parameters.color or Color.white,
			alpha = parameters.alpha or 1,
			x = padding,
			y = padding,
			w = icon_size,
			h = icon_size
		})
	end

	local _x = (icon and icon:right() or 0) + padding

	local title = new_notif:text({
		text = parameters.title or "No Title",
		font = BLTUIData.fonts.medium,
		font_size = BLTUIData.font_sizes.medium,
		x = _x,
		y = padding
	})
	self:_make_fine_text(title)

	local text = new_notif:text({
		text = parameters.text or "No Text",
		font = BLTUIData.fonts.small,
		font_size = BLTUIData.font_sizes.small,
		x = _x,
		w = new_notif:w() - _x,
		y = title:bottom(),
		h = new_notif:h() - title:bottom(),
		color = BLTUIData.colors.text,
		alpha = 0.8,
		wrap = true,
		word_wrap = true
	})

	-- Create notification data
	local data = {
		id = parameters.id,
		priority = parameters.priority or 0,
		callback = parameters.callback,
		parameters = parameters,
		panel = new_notif,
		title = title,
		text = text,
		icon = icon
	}

	-- Update notifications data
	table.insert(self._notifications, data)
	table.sort(self._notifications, function(a, b)
		return a.priority > b.priority
	end)
	self._notifications_count = table.size(self._notifications)

	-- -- Check notification visibility
	for i, notif in ipairs(self._notifications) do
		notif.panel:set_visible(i == 1)
	end
	self._current = 1

	self:_update_bars()

	return data.id
end

function BLTNotificationsGui:remove_notification(uid)
	local _, idx = self:_get_notification(uid)
	if idx then
		local notif = self._notifications[idx]
		self._content_panel:remove(notif.panel)

		table.remove(self._notifications, idx)
		self._notifications_count = table.size(self._notifications)
		self:_update_bars()
	end
end

local no_default_texture = BLT:GetGame() == "pdth" or BLT:GetGame() == "raid"
function BLTNotificationsGui:_update_bars()
	if not self._notifications_ready then return end

	if not no_default_texture then
		if not self._bar_texture then
			self._bar_texture = "guis/textures/pd2/shared_lines"
		end

		if not self._bar_bg_texture then
			self._bar_bg_texture = "guis/textures/pd2/ad_spot"
		end
	end

	-- Remove old buttons
	for i, btn in ipairs(self._buttons) do
		self._buttons_panel:remove(btn)
	end

	if self._bar then
		self._buttons_panel:remove(self._bar)
		self._bar = nil
	end

	self._buttons = {}

	local base_width_padding = math.ceil(BAR_W * (11/10))
	local dynamic_width_padding = math.floor(self._buttons_panel:w() / self._notifications_count)

	local button_width_padding = math.min(base_width_padding, dynamic_width_padding)
	self._max_bar_width = math.floor(button_width_padding / (11/10))

	-- Don't bother with the bar if we're 1 or less.
	if self._notifications_count <= 1 then return end

	local middle_button = self._notifications_count / 2 + 0.5
	local button_center_x = self._buttons_panel:w() / 2

	-- Add new notifications
	for i = 1, self._notifications_count do
		local button
		if self._bar_bg_texture then
			button = self._buttons_panel:bitmap({
				texture = self._bar_bg_texture,
				w = self._max_bar_width,
				h = BAR_H
			})
		else
			button = self._buttons_panel:rect({
				color = Color.white,
				alpha = 0.4,
				w = self._max_bar_width,
				h = BAR_H
			})
		end

		button:set_center_x(button_center_x + (i - middle_button) * button_width_padding)
		button:set_center_y((self._buttons_panel:h() - button:h()) / 2)
		table.insert(self._buttons, button)
	end

	-- Add the time bar
	if self._bar_texture then
		self._bar = self._buttons_panel:bitmap({
			texture = self._bar_texture,
			halign = "grow",
			valign = "grow",
			wrap_mode = "wrap",
			x = BAR_X,
			y = BAR_Y,
			w = self._max_bar_width,
			h = BAR_H
		})
	else
		self._bar = self._buttons_panel:rect({
			color = Color.white,
			halign = "grow",
			valign = "grow",
			wrap_mode = "wrap",
			x = BAR_X,
			y = BAR_Y,
			w = self._max_bar_width,
			h = BAR_H
		})
	end

	self:set_bar_width(self._max_bar_width, true)

	if #self._buttons > 0 then
		self._bar:set_top(self._buttons[1]:top() + BAR_Y)
		self._bar:set_left(self._buttons[1]:left() + BAR_X)
	else
		self._bar:set_visible(false)
	end
end

--------------------------------------------------------------------------------

function BLTNotificationsGui:set_bar_width(w, random)
	if not self._bar then return end

	w = w or self._max_bar_width
	self._bar_width = w

	self._bar:set_width(w)

	if self._bar_texture then
		self._bar_x = not random and self._bar_x or math.random(1, 255)
		self._bar_y = not random and self._bar_y or math.random(0, math.round(self._bar:texture_height() / 2 - 1)) * 2
		local x = self._bar_x
		local y = self._bar_y
		local h = 6
		local mvector_tl = Vector3()
		local mvector_tr = Vector3()
		local mvector_bl = Vector3()
		local mvector_br = Vector3()

		mvector3.set_static(mvector_tl, x, y, 0)
		mvector3.set_static(mvector_tr, x + w, y, 0)
		mvector3.set_static(mvector_bl, x, y + h, 0)
		mvector3.set_static(mvector_br, x + w, y + h, 0)
		self._bar:set_texture_coordinates(mvector_tl, mvector_tr, mvector_bl, mvector_br)
	end
end

function BLTNotificationsGui:_move_to_notification(destination)
	-- Animation
	local swipe_func = function(o, other_object, duration)
		if not alive(o) or not alive(other_object) then
			return
		end

		self._animating = true
		duration = duration or CHANGE_TIME
		local speed = o:w() / duration

		-- Stops the other panel from flashing on screen briefly before the while loop kicks in.
		other_object:set_x(o:right())

		o:set_visible(true)
		other_object:set_visible(true)

		while alive(o) and alive(other_object) and o:right() >= 0 do
			local dt = coroutine.yield()
			o:move(-dt * speed, 0)
			other_object:set_x(o:right())
		end

		if alive(o) then
			o:set_x(0)
			o:set_visible(false)
		end
		if alive(other_object) then
			other_object:set_x(0)
			other_object:set_visible(true)
		end

		self._animating = false
		self._current = destination
	end

	-- Stop all animations
	for _, notification in ipairs(self._notifications) do
		if alive(notification.panel) then
			notification.panel:stop()
			notification.panel:set_x(0)
			notification.panel:set_visible(false)
		end
	end

	-- Start swap animation for next notification
	local a = self._notifications[self._current]
	local b = self._notifications[destination]
	a.panel:animate(swipe_func, b.panel, CHANGE_TIME)

	-- Reset the bar width to prevent a sudden flash when click changing.
	self:set_bar_width(0)

	-- Update bar
	self._bar:set_top(self._buttons[destination]:top() + BAR_Y)
	self._bar:set_left(self._buttons[destination]:left() + BAR_X)
end

function BLTNotificationsGui:_move_notifications(dir)
	self._queued = self._current + dir
	while self._queued > self._notifications_count do
		self._queued = self._queued - self._notifications_count
	end
	while self._queued < 1 do
		self._queued = self._queued + 1
	end
end

function BLTNotificationsGui:_next_notification()
	self:_move_notifications(1)
end

function BLTNotificationsGui:update(t, dt)
	if not self._notifications_ready then return end

	-- Update download count
	local pending_downloads_count = table.size(BLT.Downloads:pending_downloads())
	if pending_downloads_count > 0 then
		self._downloads_panel:set_visible(true)
		self._downloads_count:set_text(BLTUIHelpers.nice_number_string(pending_downloads_count))
	else
		self._downloads_panel:set_visible(false)
	end

	-- Update notifications
	if self._notifications_count <= 1 then
		return
	end

	self._next_time = self._next_time or t + TIME_PER_PAGE

	if self._block_change then
		self._next_time = t + TIME_PER_PAGE
	else
		if t >= self._next_time then
			self:_next_notification()
			self._next_time = t + TIME_PER_PAGE
		end

		self:set_bar_width(self._max_bar_width * (1 - (self._next_time - t) / TIME_PER_PAGE))
	end

	if not self._animating and self._queued then
		self:_move_to_notification(self._queued)
		self._queued = nil
	end
end

--------------------------------------------------------------------------------

function BLTNotificationsGui:mouse_moved(x, y)
	if not self._enabled then
		return
	end

	if alive(self._downloads_panel) and self._downloads_panel:visible() and self._downloads_panel:inside(x, y) then
		return true, "link"
	end

	if alive(self._content_panel) and self._content_panel:inside(x, y) then
		self._content_outline:set_visible(true)
		return true, "link"
	else
		self._content_outline:set_visible(false)
	end

	for i, button in ipairs(self._buttons) do
		if button:inside(x, y) then
			return true, "link"
		end
	end
end

function BLTNotificationsGui:mouse_pressed(button, x, y)
	if not self._enabled or button ~= Idstring("0") then
		return
	end

	if alive(self._downloads_panel) and self._downloads_panel:visible() and self._downloads_panel:inside(x, y) then
		managers.menu:open_node("blt_download_manager")
		return true
	end

	if alive(self._content_panel) and self._content_panel:inside(x, y) then
		local current = self._notifications[self._current]
		if current and current.callback then
			current.callback(current.id)
		else
			managers.menu:open_node("blt_mods")
		end
		return true
	end

	if not self._animating then
		for i, button in ipairs(self._buttons) do
			if button:inside(x, y) then
				if self._current ~= i then
					self:_move_to_notification(i)
					self._next_time = Application:time() + TIME_PER_PAGE
				end
				return true
			end
		end
	end
end