---@class BLTGuiComponent
---@field new fun(self, is_start_page, component_data):BLTGuiComponent
BLTGuiComponent = BLTGuiComponent or blt_class(MenuGuiComponentGeneric)

function BLTGuiComponent:init(ws, fullscreen_ws, node)
	BLTGuiComponent.super.init(self, ws, fullscreen_ws, node)
end

function BLTGuiComponent:update(t, dt)
	if self._child_gui then
		if self._child_gui.update then
			self._child_gui:update(t, dt)
		end
	end
end

function BLTGuiComponent:close()
	if self._child_gui and self._child_gui.close then
		self._child_gui:close()
	end

	BLTGuiComponent.super.close(self)
end

function BLTGuiComponent:_setup(is_start_page, component_data)
	BLTGuiComponent.super._setup(self, is_start_page, component_data)
end

function BLTGuiComponent:mouse_moved(o, x, y)
	if managers.menu_scene and managers.menu_scene.input_focus and managers.menu_scene:input_focus() then
        return false
    end

	if self._child_gui and self._child_gui.mouse_moved then
		self._child_gui:mouse_moved(x, y)
	end
end

function BLTGuiComponent:mouse_clicked(o, button, x, y)
	if managers.menu_scene and managers.menu_scene.input_focus and managers.menu_scene:input_focus() then
        return false
    end

	if self._child_gui and self._child_gui.mouse_clicked then
		self._child_gui:mouse_clicked(button, x, y)
	end
end

function BLTGuiComponent:mouse_pressed(o, button, x, y)
	if managers.menu_scene and managers.menu_scene.input_focus and managers.menu_scene:input_focus() then
        return false
    end

	if self._child_gui and self._child_gui.mouse_pressed then
		self._child_gui:mouse_pressed(button, x, y)
	end
end

function BLTGuiComponent:mouse_released(o, button, x, y)
	if managers.menu_scene and managers.menu_scene.input_focus and managers.menu_scene:input_focus() then
        return false
    end

	if self._child_gui and self._child_gui.mouse_released then
		self._child_gui:mouse_released(button, x, y)
	end
end