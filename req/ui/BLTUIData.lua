BLTUIData = {}

if BLT:GetGame() == "pdth" then
	BLTUIData.padding = {
		general = 10,
		buttons = 10,
		mods = 10,
		downloads = 10
	}

	BLTUIData.margin = {
		top = 0,
		bottom = 0,
		left = 0,
		right = 0
	}

	BLTUIData.fonts = {
		massive = tweak_data.menu.default_font,
		large = tweak_data.menu.default_font,
		medium = tweak_data.menu.default_font,
		small = tweak_data.menu.default_font
	}

	BLTUIData.font_sizes = {
		massive = tweak_data.menu.topic_font_size + 4,
		large = tweak_data.menu.topic_font_size,
		medium = tweak_data.menu.default_font_size + 4,
		small = tweak_data.menu.default_font_size
	}

	BLTUIData.colors = {
		background = Color.black,
		title = tweak_data.menu.default_font_row_item_color,
		important = tweak_data.menu.upgrade_locked_color,
		button = tweak_data.menu.default_disabled_text_color,
		button_highlight = tweak_data.menu.highlight_background_color_right,
		text = tweak_data.menu.default_font_row_item_color
	}

	--[[ original
	BLTUIData.icons = {
		mod_enabled = {
			texture = "guis/textures/hud_icons",
			texture_rect = {
				172,
				344,
				32,
				32
			}
		},
		mod_auto_updates = {
			texture = "guis/textures/hud_icons",
			texture_rect = {
				2,
				276,
				32,
				32
			}
		},
		mod_check_updates = {
			texture = "guis/textures/hud_icons",
			texture_rect = {
				104,
				310,
				32,
				32
			}
		},
		mod_downloads = {
			texture = "guis/textures/hud_icons",
			texture_rect = {
				242,
				193,
				28,
				29
			}
		},
		scrollbar_arrow = {
			texture = "guis/textures/menu_arrows",
			texture_rect = {
				0,
				0,
				24,
				24
			},
			rotation_offset = 90
		}
	}
	]]

	BLTUIData.icons = {
		mod_enabled = {
			texture = "guis/blt/enable"
		},
		mod_auto_updates = {
			texture = "guis/blt/updates"
		},
		mod_check_updates = {
			texture = "guis/blt/check_updates"
		},
		mod_downloads = {
			texture = "guis/blt/download"
		},
		scrollbar_arrow = {
			texture = "guis/blt/scrollbar_arrows"
		}
	}

elseif BLT:GetGame() == "pd2" then
	BLTUIData.padding = {
		general = 10,
		buttons = 10,
		mods = 10,
		downloads = 10
	}

	BLTUIData.margin = {
		top = 0,
		bottom = 0,
		left = 0,
		right = 0
	}

	BLTUIData.fonts = {
		massive = tweak_data.menu.pd2_massive_font,
		large = tweak_data.menu.pd2_large_font,
		medium = tweak_data.menu.pd2_medium_font,
		small = tweak_data.menu.pd2_small_font
	}

	BLTUIData.font_sizes = {
		massive = tweak_data.menu.pd2_massive_font_size,
		large = tweak_data.menu.pd2_large_font_size,
		medium = tweak_data.menu.pd2_medium_font_size,
		small = tweak_data.menu.pd2_small_font_size
	}

	BLTUIData.colors = {
		background = Color.black,
		title = tweak_data.screen_colors.title,
		important = tweak_data.screen_colors.important_1,
		button = tweak_data.screen_colors.button_stage_3,
		button_highlight = tweak_data.screen_colors.button_stage_2,
		text = tweak_data.screen_colors.text
	}

	BLTUIData.icons = {
		mod_enabled = {
			texture = "guis/dlcs/cee/textures/pd2/crime_spree/boosts_atlas",
			texture_rect = {
				128 * 1,
				128 * 2,
				128,
				128
			}
		},
		mod_auto_updates = {
			texture = "guis/dlcs/cee/textures/pd2/crime_spree/boosts_atlas",
			texture_rect = {
				128 * 6,
				128 * 1,
				128,
				128
			}
		},
		mod_check_updates = {
			texture = "guis/dlcs/cee/textures/pd2/crime_spree/boosts_atlas",
			texture_rect = {
				128 * 1,
				128 * 0,
				128,
				128
			}
		},
		mod_downloads = {
			texture = "guis/dlcs/cee/textures/pd2/crime_spree/boosts_atlas",
			texture_rect = {
				128 * 1,
				128 * 1,
				128,
				128
			}
		},
		scrollbar_arrow = {
			texture = "guis/textures/pd2/scrollbar_arrows",
			texture_rect = {
				1,
				1,
				9,
				10
			}
		}
	}
elseif BLT:GetGame() == "raid" then
	BLTUIData.padding = {
		general = 10,
		buttons = 10,
		mods = 10,
		downloads = 10
	}

	BLTUIData.margin = {
		top = 50,
		bottom = 50,
		left = 0,
		right = 0
	}

	BLTUIData.fonts = {
		massive = tweak_data.gui.font_paths.din_compressed[42],
		large = tweak_data.gui.font_paths.din_compressed[32],
		medium = tweak_data.gui.font_paths.din_compressed[24],
		small = tweak_data.gui.font_paths.din_compressed[18]
	}

	BLTUIData.font_sizes = {
		massive = 42,
		large = 32,
		medium = 24,
		small = 18
	}

	BLTUIData.colors = {
		background = Color.black,
		title = tweak_data.screen_colors.title,
		important = tweak_data.screen_colors.important_1,
		button = tweak_data.screen_colors.heat_cold_color,
		button_highlight = tweak_data.screen_colors.heat_warm_color,
		text = tweak_data.screen_colors.text
	}

	BLTUIData.icons = {
		mod_enabled = {
			texture = "ui/hud/atlas/raid_atlas",
			texture_rect = {
				759,
				1711,
				38,
				38
			}
		},
		mod_auto_updates = {
			texture = "ui/hud/atlas/raid_atlas",
			texture_rect = {
				717,
				1717,
				32,
				32
			}
		},
		mod_check_updates = {
			texture = "ui/hud/atlas/raid_atlas",
			texture_rect = {
				977,
				1757,
				32,
				32
			}
		},
		mod_downloads = {
			texture = "ui/hud/atlas/raid_atlas",
			texture_rect = {
				720,
				1678,
				32,
				32
			}
		},
		scrollbar_arrow = {
			texture = "ui/atlas/raid_atlas_menu",
			texture_rect = {
				433,
				354,
				48,
				48
			}
		}
	}
end

BLTUIHelpers = {}

function BLTUIHelpers.nice_number_string(number)
	local sign = ""

	if number < 0 then
		sign = "-"
	end

	local total = tostring(math.round(math.abs(number)))
	local reverse = string.reverse(total)
	local s = ""

	for i = 1, string.len(reverse) do
		s = s .. string.sub(reverse, i, i) .. (math.mod(i, 3) == 0 and i ~= string.len(reverse) and managers.localization:text("cash_tousand_separator") or "")
	end

	return sign .. string.reverse(s)
end

BLT:Require("req/ui/BLTGUIBase")

BLT:Require("req/ui/BLTUIControls")
BLT:Require("req/ui/BLTModItem")
BLT:Require("req/ui/BLTScrollablePanel")

BLT:Require("req/ui/BLTViewModGui")
BLT:Require("req/ui/BLTModsGui")
BLT:Require("req/ui/BLTDownloadManagerGui")

BLT:Require("req/ui/BLTNotificationsGui")