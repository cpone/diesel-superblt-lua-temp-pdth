-- Create the menu node for BLT mods
local function add_blt_mods_node(menu)
	local new_node = {
		_meta = "node",
		name = "blt_mods",
		topic_id = "blt_installed_mods",
		back_callback = "perform_blt_save",
		gui_class = "MenuNodeGuiRaid",
		menu_components = "raid_menu_header raid_menu_footer blt_mods"
	}
	table.insert(menu, new_node)

	return new_node
end

local function add_blt_view_mod_node(menu)
	local new_node = {
		_meta = "node",
		name = "view_blt_mod",
		back_callback = "perform_blt_save",
		gui_class = "MenuNodeGuiRaid",
		menu_components = "raid_menu_footer view_blt_mod"
	}
	table.insert(menu, new_node)

	return new_node
end

local function add_blt_downloads_node(menu)
	local new_node = {
		_meta = "node",
		name = "blt_download_manager",
		topic_id = "blt_download_manager",
		gui_class = "MenuNodeGuiRaid",
		menu_components = "raid_menu_header raid_menu_footer blt_download_manager"
	}
	table.insert(menu, new_node)

	return new_node
end

BLT:Require("req/ui/components/BLTGuiComponent")
BLT:Require("req/ui/components/BLTModsComponent")
BLT:Require("req/ui/components/BLTViewModComponent")
BLT:Require("req/ui/components/BLTDownloadManagerComponent")
BLT:Require("req/ui/components/BLTNotificationsComponent")

Hooks:Add("CoreMenuData.LoadDataMenu", "BLTMenu.CoreMenuData.LoadDataMenu", function(menu_id, menu)
	log(tostring(menu_id))
	if menu_id ~= "start_menu" then
		return
	end

	for _, node in ipairs(menu) do
		log("\t" .. tostring(node.name))
		if node.name == "main" then
			log("\t\t" .. tostring(node.menu_components))
			if node.menu_components then
				node.menu_components = node.menu_components .. " blt_notifications"
			end
		end
	end

	add_blt_mods_node(menu)
	add_blt_view_mod_node(menu)
	add_blt_downloads_node(menu)
end)