_G.LoadBaseAssets = _G.LoadBaseAssets or {}
-- Load base mod assets
function LoadBaseAssets:Load()
	for _, file in pairs(file.GetFiles("mods/base/assets/guis/blt/")) do
		DB:create_entry(Idstring("texture"), Idstring("guis/blt/".. file:gsub(".texture", "")), "mods/base/assets/guis/blt/".. file)
	end
end

LoadBaseAssets:Load()