# Payday The Heist SuperBLT Beta Lua
An open source Lua hook for Payday The Heist, designed and created for ease of use for both players and modders.
This repository is for the Lua base that controls and loads mods into Payday The Heist, and the options and save files associated with the BLT. The DLL hook code can be found in it's own [repository](https://gitlab.com/cpone/diesel-superblt-lua-temp-pdth).

This is the developer repository, and should only be used if you know what you're doing.

## Installation Instructions
 - Extract the contents of Install.zip into the Payday The Heist root directory

## Changelog Beta 4
 - Added SystemFS exists and delete_file functions
 - Other small fixes

## Download Links

## Base Mod and Hook
https://gitlab.com/cpone/diesel-superblt-lua-temp-pdth/-/raw/master/Install.zip