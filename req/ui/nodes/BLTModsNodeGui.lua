---@class BLTModsNodeGui
---@field new fun(self, node, layer, parameters):BLTModsNodeGui
BLTModsNodeGui = BLTModsNodeGui or blt_class(BLTNodeGui)

function BLTModsNodeGui:_setup()
	self._child_gui = BLTModsGui:new(self._blt_panel, self.layers.items)
end