---@class BLTDownloadManagerNodeGui
---@field new fun(self, node, layer, parameters):BLTDownloadManagerNodeGui
BLTDownloadManagerNodeGui = BLTDownloadManagerNodeGui or blt_class(BLTNodeGui)

function BLTDownloadManagerNodeGui:_setup()
	self._child_gui = BLTDownloadManagerGui:new(self._blt_panel, self.layers.items)
end