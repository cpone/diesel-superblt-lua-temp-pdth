if not tweak_data then
    return
end

tweak_data.screen_colors = {
    text = Color(255, 255, 255, 255) / 255,
    resource = Color(255, 77, 198, 255) / 255,
    important_1 = Color(255, 255, 51, 51) / 255,
    important_2 = Color(125, 255, 51, 51) / 255,
    item_stage_1 = Color(255, 255, 255, 255) / 255,
    item_stage_2 = Color(255, 89, 115, 128) / 255,
    item_stage_3 = Color(255, 23, 33, 38) / 255,
    button_stage_1 = Color(255, 0, 0, 0) / 255,
    button_stage_2 = Color(255, 77, 198, 255) / 255,
    button_stage_3 = Color(127, 0, 170, 255) / 255,
    crimenet_lines = Color(255, 127, 157, 182) / 255,
    risk = Color(255, 255, 204, 0) / 255,
    friend_color = Color(255, 41, 204, 122) / 255,
    regular_color = Color(255, 41, 150, 240) / 255,
    pro_color = Color(255, 255, 51, 51) / 255,
    dlc_color = Color(255, 255, 212, 0) / 255,
    skill_color = Color(255, 77, 198, 255) / 255,
    ghost_color = Color("4ca6ff"),
    extra_bonus_color = Color(255, 255, 255, 255) / 255,
    community_color = Color(255, 59, 174, 254) / 255,
    challenge_completed_color = Color(255, 255, 168, 0) / 255,
    stat_maxed = Color("FF00FF"),
    competitive_color = Color(255, 41, 204, 122) / 255,
    event_color = Color(255, 255, 145, 0) / 255,
    infamous_color = Color(1, 0.1, 1),
    infamy_color = Color("C864C8"),
    mutators_color = Color(255, 211, 133, 255) / 255,
    mutators_color_text = Color(255, 211, 133, 255) / 255,
    crime_spree_risk = Color(255, 255, 255, 0) / 255,
    achievement_grey = Color(255, 145, 145, 145) / 255,
    skirmish_color = Color(255, 255, 85, 30) / 255,
    dlc_buy_color = Color(255, 255, 168, 0) / 255,
    heat_cold_color = Color(255, 255, 51, 51) / 255,
    heat_warm_color = Color("ff7f00"),
    heat_standard_color = Color(255, 255, 255, 255) / 255
}
tweak_data.screen_colors.heat_color = tweak_data.screen_colors.heat_standard_color
tweak_data.screen_colors.one_down = Color(255, 250, 30, 0) / 255
tweak_data.screen_colors.challenge_title = Color(255, 255, 168, 0) / 255
tweak_data.screen_colors.stats_positive = Color(255, 191, 221, 125) / 255
tweak_data.screen_colors.stats_negative = Color(255, 254, 93, 99) / 255
tweak_data.screen_colors.stats_mods = Color(255, 229, 229, 76) / 255
tweak_data.screen_colors.dark_bg = Color(0.65, 0, 0, 0)

if Global.test_new_colors then
    local r, g, b = Color.purple:unpack()

    for i, d in pairs(tweak_data.screen_colors) do
        tweak_data.screen_colors[i].r = r
        tweak_data.screen_colors[i].g = g
        tweak_data.screen_colors[i].b = b
    end
end

if Global.old_colors_purple then
    tweak_data.screen_color_white = Color.purple
    tweak_data.screen_color_red = Color.purple
    tweak_data.screen_color_green = Color.purple
    tweak_data.screen_color_grey = Color.purple
    tweak_data.screen_color_light_grey = Color.purple
    tweak_data.screen_color_blue = Color.purple
    tweak_data.screen_color_blue_selected = Color.purple
    tweak_data.screen_color_blue_highlighted = Color.purple
    tweak_data.screen_color_blue_noselected = Color.purple
    tweak_data.screen_color_yellow = Color.purple
    tweak_data.screen_color_yellow_selected = Color.purple
    tweak_data.screen_color_yellow_noselected = Color.purple
else
    tweak_data.screen_color_white = Color(1, 1, 1)
    tweak_data.screen_color_red = Color(0.7137254901960784, 0.24705882352941178, 0.21176470588235294)
    tweak_data.screen_color_green = Color(0.12549019607843137, 1, 0.5176470588235295)
    tweak_data.screen_color_grey = Color(0.39215686274509803, 0.39215686274509803, 0.39215686274509803)
    tweak_data.screen_color_light_grey = Color(0.7843137254901961, 0.7843137254901961, 0.7843137254901961)
    tweak_data.screen_color_blue = Color(0.30196078431372547, 0.7764705882352941, 1)
    tweak_data.screen_color_blue_selected = Color(0.30196078431372547, 0.7764705882352941, 1)
    tweak_data.screen_color_blue_highlighted = tweak_data.screen_color_blue_selected:with_alpha(0.75)
    tweak_data.screen_color_blue_noselected = tweak_data.screen_color_blue_selected:with_alpha(0.5)
    tweak_data.screen_color_yellow = Color(0.8627450980392157, 0.6745098039215687, 0.17647058823529413)
    tweak_data.screen_color_yellow_selected = Color(1, 0.8, 0)
    tweak_data.screen_color_yellow_noselected = Color(0.7333333333333333, 0.42745098039215684, 0.0784313725490196)
end

tweak_data.gui.DIALOG_LAYER = 1100