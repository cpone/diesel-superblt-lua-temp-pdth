---@class BLTDownloadManagerGui
---@field new fun(self, parent_panel):BLTDownloadManagerGui
BLTDownloadManagerGui = BLTDownloadManagerGui or blt_class(BLTGUIBase)

local padding = BLTUIData.padding.general
local margin = BLTUIData.margin

function BLTDownloadManagerGui:panel()
	return self._scroll_panel
end

function BLTDownloadManagerGui:_setup()
	self._scroll_panel = self._parent_panel:panel({
		layer = self._layer
	})

	local background = self._scroll_panel:rect({
		color = BLTUIData.colors.background,
		alpha = 0.4,
	})

	BLTBoxGuiObject:new(self._scroll_panel, {
		sides = {1, 1, 1, 1},
		layer = 2
	})

	BLTBoxGuiObject:new(self._scroll_panel, {
		sides = {1, 1, 2, 2},
		layer = 2
	})

	self._scroll = ScrollablePanel:new(self._scroll_panel, "downloads_scroll", {
		layer = 1
	})

	self._downloads_map = {}

	-- Add download items
	local h = 80
	for i, download in ipairs(BLT.Downloads:pending_downloads()) do
		local data = {
			y = (h + BLTUIData.padding.downloads) * (i - 1),
			w = self._scroll:canvas_scroll_width() - BLTUIData.padding.downloads,
			h = h,
			update = download.update,
			layer = self._layer
		}
		local button = BLTDownloadControl:new(self._scroll:canvas(), data, self._scroll:panel())
		table.insert(self._buttons, button)

		self._downloads_map[download.update:GetId()] = button
	end

	local num_downloads = table.size(BLT.Downloads:pending_downloads())
	if num_downloads > 0 then
		local w, h = 80, 80
		local button = BLTUIButton:new(self._scroll:canvas(), {
			x = self._scroll:canvas_scroll_width() - BLTUIData.padding.downloads - w,
			y = (h + BLTUIData.padding.downloads) * num_downloads,
			w = w,
			h = h,
			text = managers.localization:text("blt_download_all"),
			center_text = true,
			callback = callback(self, self, "clbk_download_all"),
			layer = self._layer
		}, self._scroll:panel())
		table.insert(self._buttons, button)
	end

	self._scroll:update_canvas_size()
end

function BLTDownloadManagerGui:clbk_download_all()
	BLT.Downloads:download_all()
end

function BLTDownloadManagerGui:update(t, dt)
	for _, download in ipairs(BLT.Downloads:downloads()) do
		local id = download.update:GetId()
		local button = self._downloads_map[id]
		if button then
			button:update_download(download)
		end
	end
end

function BLTDownloadManagerGui:close()
	BLT.Downloads:flush_complete_downloads()

	BLTModsGui.super.close(self)
end
